const solution = A => {
    if (!A || A.length === 0) return 1;
    return A;
};

console.log(solution([1, 2, 3]));

console.log(solution([2, 3, 4]));

console.log(solution([2, 3, 5]));
